package models

import "gorm.io/gorm"

type Reports struct {
	gorm.Model
	Num         int         `json:"num"`
	Header      string      `json:"header"`
	Description interface{} `json:"description"`
	Terms       string      `json:"terms"`
}
